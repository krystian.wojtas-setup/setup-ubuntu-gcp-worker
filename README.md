# Purpose

Install custom software and configuration for virtual machine in some cloud platform to have services:
- gitlab runner

# Usage

- clone repository into virtual machine instance
- copy template files for gitlab runner
  ```
  install -m 600 ./home/gitlab-runner/secret/gitlab-runner/url{.template,}
  install -m 600 ./home/gitlab-runner/secret/gitlab-runner/token{.template,}
  ```
- set gitlab secret url and token
  ```
  nano ./home/gitlab-runner/secret/gitlab-runner/url
  nano ./home/gitlab-runner/secret/gitlab-runner/token
  ```
- run installer
  ```
  ./install
  ```

# Testing

It could be run locally to test virtual machine instance
```
vagrant up
```

If there is following [known](https://github.com/dotless-de/vagrant-vbguest/issues/278) error
```
Got different reports about installed GuestAdditions version:
Virtualbox on your host claims:   5.2.8
VBoxService inside the vm claims: 5.2.32
Going on, assuming VBoxService is correct...
[default] GuestAdditions seems to be installed (5.2.32) correctly, but not running.
Got different reports about installed GuestAdditions version:
Virtualbox on your host claims:   5.2.8
VBoxService inside the vm claims: 5.2.32
Going on, assuming VBoxService is correct...
Failed to start vboxadd.service: Unit vboxadd.service not found.
Failed to start vboxadd-service.service: Unit vboxadd-service.service not found.
Got different reports about installed GuestAdditions version:
Virtualbox on your host claims:   5.2.8
VBoxService inside the vm claims: 5.2.32
Going on, assuming VBoxService is correct...
bash: line 4: setup: command not found
==> default: Checking for guest additions in VM...
The following SSH command responded with a non-zero exit status.
Vagrant assumes that this means the command failed!

 setup

Stdout from the command:



Stderr from the command:

bash: line 4: setup: command not found
```
then run provisiong once more time
```
vagrant up --provision
```
